= {page-component-title} {page-component-version}

== Page Coordinates

page-component-name: {page-component-name}

page-component-version: {page-component-version}

page-module: {page-module}

page-relative: {page-relative}


== A page

And some content.

== Links

xref:_@component-a::index.adoc[]

xref:1.0@component-a::index.adoc[]

xref:_@index.adoc[]

//xref:1.0@component-b:ROOT:index.adoc[]
